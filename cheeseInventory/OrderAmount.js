function myOrder(dayOfWeek, amountLeft) {
    this.dayOfWeek = dayOfWeek;
    this.amountLeft = amountLeft;
}

module.exports.order = {
    lastWeekLeft: 21,
    days : ["Monday", "Tuesday", "Wednesday", "Thursday", "Saturday", "Sunday"],
    howMuchToOrder: function (dayOfWeek, amountLeft){
        myOrder.call(this, dayOfWeek, amountLeft);
        ///Input Validation
        if ((typeof this.dayOfWeek !== 'string') && (!Number.isFinite(this.amountLeft))){
            return "Check your inputs again, probably in the wrong order"
        }if ((typeof this.dayOfWeek !== 'string')){
            return "Invalid Date Given"
        } if((!Number.isFinite(this.amountLeft)) || this.amountLeft === null){
            return "Amount Given is not a Number or null."
        } if(this.dayOfWeek !== "Friday" && this.days.includes(this.dayOfWeek)){
            return "Not time to order."
        } if(this.amountLeft < 0 && this.dayOfWeek === "Friday" || this.days.includes(this.dayOfWeek)){
            return "Invalid amount"
        }
        ///Order Logic
        if (this.dayOfWeek === "Friday" && this.amountLeft <= 0.99 && this.amountLeft >= 0.00){
            return "You should order 15 lbs of cheese."
        } if(this.dayOfWeek === "Friday" && this.amountLeft <= 10.00 && this.amountLeft >= 1.00){
            return "You should order 5 lbs of cheese."
        } if (this.dayOfWeek === "Friday" && this.amountLeft > 10.00){
            return "You should order "+(this.lastWeekLeft - 5.00)+" lbs of cheese."
        } else {
            return "Error!"
        }
    }
};