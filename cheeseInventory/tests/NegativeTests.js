const expect = require('chai').expect;
const cheeseMethod = require('../OrderAmount');

describe('Cheesiest Negative tests', function () {
    it('Negative Number', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", -0.01)).to.equal("Invalid amount");
        done()
    });
    it('Random String', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("blah", 5)).to.equal("Error!");
        done()
    });
    it('Negative Number and Random String', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("boo", -23)).to.equal("Error!");
        done()
    });
    it('Passing Integer in Day field', function (done) {
        expect(cheeseMethod.order.howMuchToOrder(12, -5)).to.equal("Invalid Date Given");
        done()
    });
    it('Passing string in Amount field and Passing Integer in Day field', function (done) {
        expect(cheeseMethod.order.howMuchToOrder(12, "Monday")).to.equal("Check your inputs again, probably in the wrong order");
        done()
    });
    it('Leaving Amount Empty', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Monday")).to.equal("Amount Given is not a Number or null.");
        done()
    });
    it('Passing String in Amount Field', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday","Monday")).to.equal("Amount Given is not a Number or null.");
        done()
    });
});