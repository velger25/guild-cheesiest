const expect = require('chai').expect;
const cheeseMethod = require('../OrderAmount');
let nonOrderDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Saturday", "Sunday"];

nonOrderDays.forEach(function(nonOrderDays) {
    describe('Cheese Orders', function () {
        it('Non Order Days', function (done) {
            expect(cheeseMethod.order.howMuchToOrder(nonOrderDays, 0)).to.equal("Not time to order.");
            done()
        });
    })
});