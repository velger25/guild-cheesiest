const expect = require('chai').expect;
const cheeseMethod = require('../OrderAmount');

describe('Cheesiest Boundry tests', function () {
    it('0.10lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 0.01)).to.equal("You should order 15 lbs of cheese.");
        done()
    });
    it('0.99lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 0.99)).to.equal("You should order 15 lbs of cheese.");
        done()
    });
    it('1lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 1.00)).to.equal("You should order 5 lbs of cheese.");
        done()
    });
    it('9lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 9.99)).to.equal("You should order 5 lbs of cheese.");
        done()
    });
    it('10.01lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 10.01)).to.equal("You should order 16 lbs of cheese.");
        done()
    });
});