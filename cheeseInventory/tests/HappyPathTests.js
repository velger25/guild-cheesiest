const expect = require('chai').expect;
const cheeseMethod = require('../OrderAmount');

describe('Cheesiest Happy Path', function () {
    it('0lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 0.00)).to.equal("You should order 15 lbs of cheese.");
            done()
        });
    it('0lbs left No Decimals', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 0)).to.equal("You should order 15 lbs of cheese.");
        done()
    });
    it('5lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 5.00)).to.equal("You should order 5 lbs of cheese.");
        done()
    });
    it('5lbs left No Decimals', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 5)).to.equal("You should order 5 lbs of cheese.");
        done()
    });
    it('11lbs left', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 11.00)).to.equal("You should order 16 lbs of cheese.");
        done()
    });
    it('11lbs left No Decimals', function (done) {
        expect(cheeseMethod.order.howMuchToOrder("Friday", 11)).to.equal("You should order 16 lbs of cheese.");
        done()
    });
});